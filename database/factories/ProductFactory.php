<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'slug' => $faker->slug,
        'featured' => false,
        'details' => $faker->sentence(8),
        'price' => $faker->numberBetween(1000, 500000),
        'description' => $faker->paragraph,
        'image' => 'products/dummy/230x180.png',
        'images' => '["products\/dummy\/230x180.png","products\/dummy\/230x180.png","products\/dummy\/230x180.png"]',
        'quantity' => 10,
    ];
});
